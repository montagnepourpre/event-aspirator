import pytest

from back.facebook_crawler import EventGetter, UnreachableFacebookPageException

# def test_get_events_from_alternatiba_rhone_return_good_list_of_event():
#     event_getter = EventGetter()
#     events = event_getter.get_events_from_page('alternatiba.rhone')
#     assert events == ['https://mbasic.facebook.com/events/210848240542571',
#                       'https://mbasic.facebook.com/events/757455691856052',
#                       'https://mbasic.facebook.com/events/409161940155547',
#                       'https://mbasic.facebook.com/events/413883353001852']

# def test_get_events_from_page_of_unreachable_page_raise_UnreachableFacebookPageException():
#     event_getter = EventGetter()
#     with pytest.raises(UnreachableFacebookPageException):
#         event_getter.get_events_from_page('2a5685adc8a42a38812caf7e82be2695a8d0de69a00955b32f9bdebaf3a7944b078f9f78e6e07a7f12633dbf0b91b95e')
