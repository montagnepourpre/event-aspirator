import re
import json
from multiprocessing import Pool, TimeoutError
from typing import List

import requests
import xml.etree.ElementTree as ET

from .exception import FacebookCrawlerException

class UnreachableFacebookPageException(FacebookCrawlerException):
    pass

def _extract_link_from_context(link_with_context):
    """
    remove the context from the event link to prevent facebook to track you
    """
    return re.match(r'\/events\/[0-9]*', link_with_context).group(0)

class Event:

    def __init__(self):
        self.event_url = ''
        self.title = ''
        self.location_name = ''
        self.location_address = ''
        self.date = ''
        self.description = ''
        self.image = ''

    def __str__(self):
        return self.image
        
    def to_json(self):
        d = {
            # 'event_url': self['event_url,
            'title': self.title,
            'location_name': self.location_name,
            'location_address': self.location_address,
            'date': self.date,
            'description': self.description,
            'image': self.image,
        }
        return json.dumps(d).replace('\\\\', '\\')
    
base_request = './/{http://www.w3.org/1999/xhtml}div[@id="root"]/{http://www.w3.org/1999/xhtml}table/{http://www.w3.org/1999/xhtml}tbody/{http://www.w3.org/1999/xhtml}tr/{http://www.w3.org/1999/xhtml}td'
image_request = './/{http://www.w3.org/1999/xhtml}div[@id="root"]/{http://www.w3.org/1999/xhtml}table/{http://www.w3.org/1999/xhtml}tbody/{http://www.w3.org/1999/xhtml}tr/{http://www.w3.org/1999/xhtml}td/{http://www.w3.org/1999/xhtml}div/{http://www.w3.org/1999/xhtml}div/{http://www.w3.org/1999/xhtml}a/{http://www.w3.org/1999/xhtml}img'
title_request = base_request + '/{http://www.w3.org/1999/xhtml}div[3]/{http://www.w3.org/1999/xhtml}div[2]/{http://www.w3.org/1999/xhtml}div/{http://www.w3.org/1999/xhtml}h3'
location_name_request = base_request + '/{http://www.w3.org/1999/xhtml}div[4]/{http://www.w3.org/1999/xhtml}div/{http://www.w3.org/1999/xhtml}div[2]/{http://www.w3.org/1999/xhtml}table/{http://www.w3.org/1999/xhtml}tbody/{http://www.w3.org/1999/xhtml}tr/{http://www.w3.org/1999/xhtml}td[2]/{http://www.w3.org/1999/xhtml}dt/{http://www.w3.org/1999/xhtml}div'
location_address_request = base_request + '/{http://www.w3.org/1999/xhtml}div[4]/{http://www.w3.org/1999/xhtml}div/{http://www.w3.org/1999/xhtml}div[2]/{http://www.w3.org/1999/xhtml}table/{http://www.w3.org/1999/xhtml}tbody/{http://www.w3.org/1999/xhtml}tr/{http://www.w3.org/1999/xhtml}td[2]/{http://www.w3.org/1999/xhtml}dd/{http://www.w3.org/1999/xhtml}div'
date_request = base_request + '/{http://www.w3.org/1999/xhtml}div[4]/{http://www.w3.org/1999/xhtml}div/{http://www.w3.org/1999/xhtml}div[1]/{http://www.w3.org/1999/xhtml}table/{http://www.w3.org/1999/xhtml}tbody/{http://www.w3.org/1999/xhtml}tr/{http://www.w3.org/1999/xhtml}td/{http://www.w3.org/1999/xhtml}dt/{http://www.w3.org/1999/xhtml}div'
description_request = base_request + '/{http://www.w3.org/1999/xhtml}table/{http://www.w3.org/1999/xhtml}tbody/{http://www.w3.org/1999/xhtml}tr/{http://www.w3.org/1999/xhtml}td/{http://www.w3.org/1999/xhtml}div/{http://www.w3.org/1999/xhtml}div[2]/{http://www.w3.org/1999/xhtml}div[2]/{http://www.w3.org/1999/xhtml}div[2]'
    
def gen_event_from_url(url):
    r = requests.get(url)
    page_text = re.sub(r'image":"[^"]*', '', r.text)
    page_text = re.sub(r'&nbsp;', '', page_text)
    root = ET.fromstring(page_text)
    img = root.findall(image_request)[0]
    event = Event()
    event.image = img.attrib['src']
    event.description = root.findall(description_request)[0].text
    event.title = root.findall(title_request)[0].text
    event.location_name = root.findall(location_name_request)[0].text
    event.location_address = root.findall(location_address_request)[0].text
    event.date = root.findall(date_request)[0].text
    return event


page_image_request = '//div[@id="root"]/div[2]/div/div/div/div/div/a/img/@src'
# page_image_request = '//div[@id="root"]/div'

class FacebookCrawler:

    def __init__(self, facebook_url : str = 'https://mbasic.facebook.com'):
        """
        :param facebook_url: facebook url
        """
        self.facebook_url = facebook_url

    def _get_events_url_from_page(self, page_name: str) -> List[str]:
        """
        crawl a facebook page and return links to all public event organised by this page
        :param page_name: page name (for facebook.com/PAGE the page name is PAGE)
        :return: a list of url to the event
        """
        url = self.facebook_url + '/' + page_name + '/events'
        
        event_list_request = './/{http://www.w3.org/1999/xhtml}div[@id="root"]/{http://www.w3.org/1999/xhtml}table/{http://www.w3.org/1999/xhtml}tbody/{http://www.w3.org/1999/xhtml}tr/{http://www.w3.org/1999/xhtml}td/{http://www.w3.org/1999/xhtml}div[2]/{http://www.w3.org/1999/xhtml}div/{http://www.w3.org/1999/xhtml}div/{http://www.w3.org/1999/xhtml}div[2]/{http://www.w3.org/1999/xhtml}div/{http://www.w3.org/1999/xhtml}table/{http://www.w3.org/1999/xhtml}tbody/{http://www.w3.org/1999/xhtml}tr/{http://www.w3.org/1999/xhtml}td/{http://www.w3.org/1999/xhtml}div'
        event_link_request = './{http://www.w3.org/1999/xhtml}div/{http://www.w3.org/1999/xhtml}span[3]/{http://www.w3.org/1999/xhtml}div/{http://www.w3.org/1999/xhtml}a'
        event_link_list = []
        r = requests.get(url)
        page_text = r.text.encode()
        root = ET.fromstring(page_text)
        for event_xml in root.findall(event_list_request):
            link = event_xml.findall(event_link_request)[0]
            event_link_list.append(_extract_link_from_context(link.attrib['href']))
        return event_link_list

    def _get_events_from_urls(self, events_url: List[str]) -> List[Event]:
        with Pool(processes=1) as pool:
            res = pool.apply_async(_crawl_events, (events_url,))
            return res.get()
        
    def get_events_from_page(self, page_name: str) -> List[Event]:
        """
        crawl a facebook page and return a list of its events
        :param page_name: page name (for facebook.com/PAGE the page name is PAGE)
        :return: a list of event
        """
        events = []
        events_url = self._get_events_url_from_page(page_name)
        for url in events_url:
            full_url = self.facebook_url + url
            events.append(gen_event_from_url(full_url))
        return events
    
    def get_image_from_page(self, page_name: str) -> str:

        image_request = './/{http://www.w3.org/1999/xhtml}div[@id="root"]/{http://www.w3.org/1999/xhtml}div[2]/{http://www.w3.org/1999/xhtml}div/{http://www.w3.org/1999/xhtml}div/{http://www.w3.org/1999/xhtml}div/{http://www.w3.org/1999/xhtml}div/{http://www.w3.org/1999/xhtml}div/{http://www.w3.org/1999/xhtml}a/{http://www.w3.org/1999/xhtml}img'
        url = self.facebook_url + '/' + page_name
        r = requests.get(url)
        page_text = r.text.encode()

        root = ET.fromstring(page_text)
        img = root.findall(image_request)[0]
        return img.attrib['src']
