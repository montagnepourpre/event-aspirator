import time
import json
from .facebook_crawler import FacebookCrawler
from flask import Flask, request, make_response

app = Flask(__name__)

fbc = FacebookCrawler()

page_list = ['alternatiba.rhone']

def to_json(page_name):
    event_list = fbc.get_events_from_page(page_name)
    image = fbc.get_image_from_page(page_name)
    json = '{"name":"' + str(page_name) + '","image":"' + str(image) + '","events":['
    for event_json in map(lambda e: e.to_json(), event_list):
        json += event_json + ','
    json = json[:-1]
    json += ']}'
    return json

final_json = '{"pages":['

for page in map(to_json, page_list):
    final_json += page + ','
final_json = final_json[:-1]
final_json += ']}'
data_json = final_json

@app.route('/', methods=['GET'])
@app.route('/<pageName>', methods=['GET'])
def getPage(pageName=None):
    data = json.loads(data_json)
    if pageName:
        # Get one page
        for page in data['pages']:
            if page['name'] == pageName:
                return data['pages'][0]
        return {}
    else:
        # Get all pages
        return data

@app.route('/add', methods=['PUT'])
def addPage():
    # Check if page start by "http[s]://facebook.com/(.*)"
    return "Page added: " + str(request.data)
