$(function() {
  $.ajax({
    headers: { "Accept": "application/json"},
    type: 'GET',
    url: 'https://event-aspirator.space/data',
    crossDomain: true,
    success: function(data, textStatus, request){
      data.pages.forEach((page) => {
        var pageTitle = $('<h3 class="pageTitle text-center">'+page.name+'<h3>');
        var pageImage = $('<img src="'+page.image+'" class="card-img-top page-img" alt="page img"/>');
        var pageEventList = $('<div class="eventsList row"></div>');
    
        page.events.forEach((event) => {
          var eventTitle = $('<h5 class="card-title">'+event.title+'</h5>');
          var eventDescription = $('<p class="card-text">'+event.description+'</p>');
          var eventLocalisation = $('<p class="card-text"><small class="text-muted">'+
            event.location_name+' - '+event.location_address+'</small></p>');
          var eventDate = $('<p class="card-text"><small class="text-muted">'+
            event.date+'</small></p>');
          var eventBody = $('<div class="card-body"></div>')
            .append(eventTitle)
            .append(eventDescription)
            .append(eventLocalisation)
            .append(eventDate);
    
          var eventImg = $('<img src="'+event.image+'" class="card-img-top event-img" alt="event img" />')
          var eventCard = $('<div class="event card p-0"></div>');
          eventCard.append(eventImg).append(eventBody);
          pageEventList.append(eventCard);
        });
    
        // Ajouter l'ensemble dans le DOM
        var finalDiv = $('<div class="page card col"></div>');
        finalDiv.append(pageImage).append(pageTitle).append(pageEventList);
        $('#events').append(finalDiv);
      });
    }
  });
});
