![Logo de Event Aspirator](front/img/EventAspirator.png)

Event Aspirator is a tool to follow the events of pages on facebook without having a facebook account.

It parses certain facebook pages to extract events information and stock them in JSON files. These information are used to publish the events on our website but can also be used for anyone to create a RSS feed.

# How does it work

We use the [mbasic version of facebook](mbasic.facebook.com) to have all the information of the events directly in the html page.

We parse the html with homemade regex.

We then send the info in a JSON format through a socket to the front-end.

# Why did we do this

We explained [here](https://event-aspirator.space/) (or in index.html - in french) the political reasons for this tool.

The other reason is that we wanted to play around and code something together during a week-end, that's why lots of stuff are handmade when we could maybe have used other existing solutions.

That's also the reason for the name. 1 minute to find it. Don't judge. :D
